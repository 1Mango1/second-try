#include <iostream>
#include <string>
using namespace std;

void Program1()
{
    int count = 0;
    while ( count < 21 )
    {
        cout << count++ << " ";
    }
}

void Program2()
{
    int a = 1;
    while ( a <= 64 )
    {
        cout << (a = a * 2) << " ";
    }

}

void Program3()
{
    int secretNumber = 6;
    cout << "Guess a number: ";
    int playerGuess;
    do
    {
        cin >> playerGuess;
        if (playerGuess > secretNumber)
        {
            cout << "Too high!" << endl;
            cout << " " << endl;
            cout << "Guess a number: ";
        }
        if (playerGuess < secretNumber)
        {
            cout << "Too low!" << endl;
            cout << " " << endl;
            cout << "Guess a number: ";
        }
        if (playerGuess == secretNumber)
        {
            cout << "Thats Right!" << endl;
        }
    } while (playerGuess != secretNumber);

    cout << "GAME OVER";
}

void Program4()
{
    cout << "Please enter a  number between 1 and 5: ";
    int number;
    cin >> number;
    do
    {

        if (number <=5 && number >= 1)
        {
            cout << "Thank you.";
        }
        else
        {
            cout << "Invalid entry, try again: ";
            cin >> number;
        }
    } while (number > 5 || number < 1);
}

void Program5()
{
    cout << "Starting Wage: ";
    float startingWage;
    cin >> startingWage;

    cout << "Percent Raise Per Year: ";
    float percentRaisePerYear;
    cin >> percentRaisePerYear;

    float adjustedWage = startingWage;

    cout << "Years Worked: ";
    int yearsWorked;
    cin >> yearsWorked;

    int yearsCounter = 0;
    do
    {
        cout << "Salary at year " << yearsCounter++ << ": " << (adjustedWage * percentRaisePerYear / 100) + adjustedWage << endl;
    } while (yearsWorked >= yearsCounter);
}

void Program6()
{
    int n;
    int counter = 1;
    int sum;
    cout << "Enter value for n: ";
    cin >> n;

    do
    {
        cout << "Sum: " << (sum += counter++) - 8 << endl;
    } while (counter <= n);

}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
